# Tident Rodeo - Vagrant VM

Can be used to create a Vagrant box for the development of the Rodeo project.

This also takes advantage of Git sumbmodules to be able to restore the production database for rodeo.

After cloning this repo you will need to initialize the submodule.

```
git submodule init
git submodule update
```
You may need to checkout the right branch of the submodule, depending on the current state of the repo. The submodule will be in the files/webapp directory

```
cd files/webapp
git checkout <branch>
```

To buld the VM you will need [Packer](https://www.packer.io/intro/getting-started/setup.html) installed and [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

To start the build	you need to run packer on the included tempalte.

```
cd template
packer build tempalte.json
```

The built box will be in the ``build`` folder in the top level directory.

