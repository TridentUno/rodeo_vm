#!/bin/bash
#
# Setup the the box. This runs as root

apt-get -y update
apt-get -y install curl

# Remove configuration-management systems preinstalled in official Ubuntu images
apt-get -y remove --purge chef chef-zero puppet puppet-common landscape-client landscape-common

# remove any dependencies
apt-get -y autoremove

# add the PostgreSQL repository
echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# pre accept the ms fonts eula
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

# install the hte software-properties utils to make adding the ppa easier
sudo apt-get install -y software-properties-common python-software-properties

# add the ppa for libreoffice
sudo add-apt-repository -y ppa:libreoffice/libreoffice-4-4

# Get any security updates not in the base image
sudo apt-get update
sudo apt-get -y dist-upgrade

# Other packages we need
sudo apt-get install -q -y git vim nodejs postgresql-9.4 libpq-dev postgis redis-server golang postgresql-9.4-postgis-2.1 nginx libxslt1-dev libxml2-dev libreoffice-common pdftk unoconv ttf-mscorefonts-installer

# Copy our files into place
sudo rsync -rtv /tmp/files/etcfiles/ /etc
rsync -rtv /tmp/files/binfiles/ /usr/local/bin
# Force MOTD generation (will only work on 14.04)
rm -f /etc/update-motd.d/51-cloudguest
run-parts --lsbsysinit /etc/update-motd.d > /run/motd.dynamic

# update the ownership of the postgres files
chown -R postgres /etc/postgresql/9.4/main/

# restart postgres for the config files changed to take place
service postgresql restart
service nginx restart

# add the vagrant db user
sudo -H -u postgres psql -d postgres -c "create user vagrant with password 'vagrant' superuser"

# install the following as root
su -l vagrant <<'EOF'
# Install RVM
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm

# ignore documentation
echo "install: --no-rdoc --no-ri" >> ~/.gemrc 
echo "update:  --no-rdoc --no-ri" >> ~/.gemrc

# add the postgis utils to the path
export PATH=$PATH:/usr/share/postgresql/9.4/contrib/postgis-2.1/

# install the rails 
rvm install 2.2.2
rvm use 2.2.2@global --default
gem install bundler

# prep the database by cloning from the remote
cd /tmp/files/webapp/
bundle install
rake db:create
rake db:rebuild_from_remote

# copy the uploaded assets from the production server
mkdir ~/tmp
rsync -rtv /tmp/files/system.tar.gz ~/tmp/

# Copy files that should be owned by the user account
rsync -rtv /tmp/files/dotfiles/ /home/vagrant
chmod 0600 ~/.ssh/id_rsa

# Our bash setup will cd to workspace on login.
ln -s /vagrant ~/workspace
EOF

# You can install anything you need here.
